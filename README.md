# tf-eks-mg-demo

$ `git clone https://gitlab.com/mlabtek/tf-eks-mg-demo.git`

# Prerequisites

 - Terraform 

 - AWS account

 - AWS CLI 

 - kubectl

 - helm

# Deploy EKS with terraform

$ `cd tf-eks-mg-demo/deploy-eks` \
$ `terraform init` \
$ `terraform plan` \
$ `terraform apply` \
$ `cd ../..`


# Deploy MongoDB cluster with terraform 


$ `cd tf-eks-mg-demo/deploy-mongodb` \
$ `terraform init` \
$ `terraform plan` \
$ `terraform apply` \
$ `cd ../..`

# Connectivity test

$ `cd tf-eks-mg-demo/deploy-eks` \
$ `aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)` 

$ `export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace mongodb mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 -d)`

$ `kubectl run --namespace mongodb mongodb-client --rm --tty -i --restart='Never' --env="MONGODB_ROOT_PASSWORD=$MONGODB_ROOT_PASSWORD" --image docker.io/bitnami/mongodb:6.0.4-debian-11-r14 --command -- bash`

$ `mongosh admin --host "mongodb-0.mongodb-headless.mongodb.svc.cluster.local:27017,mongodb-1.mongodb-headless.mongodb.svc.cluster.local:27017" --authenticationDatabase admin -u root -p $MONGODB_ROOT_PASSWORD`

$ `show databases`

